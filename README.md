# Hi! I'm Cameron MacDonald!

I'm a web developer, UX designer, and avid geek hobbyist.

Certified Professional of Accessibility Core Competencies (CPACC) from IAAP
UX Certified from Nielsen Norman Group (NN/g)

## Skills

- HTML/CSS
- JavaScript & TypeScript
- React
- Astro
- Express.js
- Node.js
- Jenkins
- Figma
- Sketch

## Hobbies

- 3D Modeling & Sculpting (Blender - FOSS bay-bee!)
- 3D Printing
- 2D Illustration & Animation
- Piano & Guitar
- Video Games
- Cycling
- Skiing
- General Tomfoolery

## Topics of Passion
- Design
- User experience
- Accessibility (Digital and analog)
- Climate change
- Education
- Public transportation
- Urban design
